/*
    Saionara Coil Holder/Organizer with lid and tool
*/
coil_holder(3, 2);
/*

	Create the grid of coil holders you would like and optionally include a lid
	and tool to help screw the coils into / out of the holder. The below are
	optional flags to include / exclude each component :
	- body		|  Setting this to false will exclude the main body. Can be used
				|  to print the lid or tool independent of the body.

	- tool		|  Default is true, if set the false the tool will not be
				|  included

	- lid		|  If set to false the lid will not be included and the body
				|  will default to not having a hinge. Set the hinge flag to
				|  true if you want to body to print with a hinge despite the
				|  lid flag value

	- hinge     |  Only determines if the hinge appears on the body when the lid
				|  flag is set to false

	- coil		|  Sets the coil profiles listed in the coil profile section.
				|  Default value is saionara.

	The default configuration will generate all components for a holder of
	specified dimensions. For example:
	A 3 x 5 holder with lid and tool
		coil_holder(3, 5);
	A 4 x 4 holder without a lid or tool
		coil_holder(4, 4, lid=false, tool=false);
	A 2 x 3 holder body with a hinge but without a lid
		coil_holder(2, 3, lid=false, hinge=true);
	A lid for a 4 x 5 holder without a body or tool
		coil_holder(4, 5, body=false, tool=false);
*/


/*
	+-----------------------------------------------------------------------+
	|	Coil profiles														|
	+-----------------------------------------------------------------------+

	Defined as [name] = [A, B, C, D, E, F];
	A - Top (blue) section diameter
	B - Top (blue) section height
	C - Middle (pink) section diameter
	D - Middle (pink) section height
	E - Bottom (purple) section diameter
	F - Bottom (purple) section height
*/
saionara = [15.25, 12.55, 12.25, 3.8, 6.95, 7.3];


/*
	+-----------------------------------------------------------------------+
	|	Global Values														|
	+-----------------------------------------------------------------------+

	The below values define tolerances and other static values such as the
	hinge sizes, coil clearances and spacing, spacing between each part, the holder radius, and lid thicknesses.
*/

//	Increase the granularity of our rounded sections
$fn=64;

//	Define the thickness of the bottom
$b_thickness = 2;

//	Define the thickness of the cover
$lid_top_thickness = 3;
$lid_side_thickness = 2;

//	Set the coil clearance
$coil_clearance = 3;
$coil_spacing = 3;

//	Set the corner radius
$corner_radius = 3;

//	Spacing between parts (holder, lid, tool)
$part_spacing = 5;

//	Hinge size configuration
$hinge_size = 10;
$hinge_thickness = 5;
$hinge_pin = 3;
$hinge_padding = 0.01;
$hinge_clearance = 0.05;


/*
	+-----------------------------------------------------------------------+
	|	Generic Modules														+
	+-----------------------------------------------------------------------+

	The below modules are generic and used to help generate reusable shapes
	that are leveraged by our coil, lid, tool and holder specific modules
*/

//	Generic rounded shell module
module rounded_shell(width, depth, height, radius){
	hull(){
		translate([radius,radius, 0])cylinder(r = radius,h = height);
		translate([depth - radius,radius, 0])cylinder(r = radius,h = height);
		translate([radius,width - radius, 0])cylinder(r = radius,h = height);
		translate([depth - radius,width - radius, 0])cylinder(r = radius,h = height);
	}
}

//	ngon module for creating polygons of a given size
module ngon(num, r) {
    polygon([for (i = [0 : num - 1], a = i * 360 / num) [r * cos(a), r * sin(a) ]]);
}


/*
	+-----------------------------------------------------------------------+
	|	Tool Module															+
	+-----------------------------------------------------------------------+

	The below module creates our coil tool which also serves as our lid
	hinge pin.

	TODO:	Add the measurements to the saionara object and pull them in to
			tool creation so it can suppor the sequoia as well
*/

//	Coil tool
module tool(depth, body_width){
	inner_width = ((depth * body_width) - ($corner_radius * 2));
    knuckle_count = (inner_width - (inner_width % $hinge_thickness)) / $hinge_thickness;
	translate([-7.5, 5.8, 0]){
		cylinder(d1 = 9.8, d2 = 11.6, h = 3.3);
		translate([0, 0, 1.65]){
			cube([15, 1.7, 3.3], true);
		}
		translate([0,0,3]){
			cylinder(d=cos(30) * ($hinge_pin - 0.1) * 2, h=depth * body_width + $lid_side_thickness * 2);
			translate([0, 0, $lid_side_thickness + (-knuckle_count * $hinge_thickness + depth * body_width) / 2]){
				for(i = [0 : knuckle_count - 1]){
					translate([0, 0, $hinge_thickness * i]){
						if(i % 2 == 0){
							linear_extrude(height = $hinge_thickness - $hinge_clearance * 2)ngon(6, $hinge_pin - 0.1);
						}
					}
				}
			}
		}
	}
}


/*
	+-----------------------------------------------------------------------+
	|	Hinge Modules														+
	+-----------------------------------------------------------------------+

	The below modules define the knuckles for our hinges. Sizes are set in
	the Global Values section. By default, we create a hexagon shaped pin
	so that a locking pin can be used and no leafs are generated
*/

//	Hinge knuckle
module hinge_knuckle(orientation){

    rotation = orientation == "side" ? [90, 0, 180] : [90, 270, 180];
    pin_rotation = orientation == "side" ? [0, 0, 0] : [0, 0, 30];

    translate([$hinge_size / 2, 0, -$hinge_size / 2])rotate(rotation){
        difference(){
            union(){
            	cylinder(d = $hinge_size, h = $hinge_thickness - $hinge_clearance);
                translate([$hinge_size / 4, 0, ($hinge_thickness - $hinge_clearance) / 2]){
                    cube([$hinge_size / 2, $hinge_size, $hinge_thickness - $hinge_clearance], center = true);
                }
            }
            translate([0, 0, -0.01]){
                linear_extrude(height = $hinge_thickness + 0.02)rotate(pin_rotation){
					ngon(6, $hinge_pin + $hinge_padding);
				}
            }
        }
    }
}

//	Hinge collection
module hinge(depth, width, orientation, body_width){
    inner_width = ((depth * body_width) - ($corner_radius * 2));
    knuckle_count = (inner_width - (inner_width % $hinge_thickness)) / $hinge_thickness;
    translate([0,-knuckle_count * $hinge_thickness / 2,0]){
        union()for(i = [0 : knuckle_count - 1]){
            translate([0, $hinge_thickness * i, 0]){
                if(orientation == "side" && i % 2 == 0){
                    hinge_knuckle(orientation);
                }
                if(orientation != "side" && i % 2 == 1){
                    hinge_knuckle(orientation);
                }
            }
        }
    }
}


/*
	+-----------------------------------------------------------------------+
	|	Lid Modules															|
	+-----------------------------------------------------------------------+

	The below templates define our lid negative and final lid shape. Soon
	multiple lid types will be supported including slide and magnetic

	TODO:	Add support for different lid types - slide, hinge, and magnetic
*/

//	Lid Negative Space
module lid_negative(depth, width, height, body_width){
	translate([0, 0, $coil_clearance / 2]){
		cube([body_width * width, body_width * depth, $coil_clearance], true);
	}
}

//	Create the lid for the coil holder
module lid(depth, width, height, body_width){
	$lid_width = body_width * width + $hinge_size + $lid_side_thickness * 2;
	$lid_depth = body_width * depth + $lid_side_thickness * 2;
	$lid_insert_width = body_width * width;
	$lid_insert_depth = body_width * depth;
	union(){
		intersection(){
			difference(){
				cube([$lid_width, $lid_depth, $lid_top_thickness + $coil_clearance]);
				translate([$lid_insert_width / 2 + $lid_side_thickness,$lid_depth / 2, -0.01]){
					lid_negative(depth, width, height, body_width);
				}
			}
			rounded_shell($lid_depth, $lid_width, $lid_top_thickness + $coil_clearance, $corner_radius);
		}
		translate([body_width * width + $lid_side_thickness * 2,(body_width * depth + $lid_side_thickness * 2) / 2, 0]){
			hinge(depth, width, "bottom", body_width);
		}
	}
}


/*
	+-----------------------------------------------------------------------+
	|	Coil Modules														|
	+-----------------------------------------------------------------------+

	The below templates define the individual coil_pockets, the grid of all
	coil pockets, and our final coil_holder
*/

//	Coil Pocket Negative
module coil_indent(coil) {
	// Create the highest tier cylinder
	color("#00ddff")cylinder(d = coil[0], h = coil[1]);

	// Create our second tier and move it beneath the first
	translate([0, 0, 0.01 - coil[3]]) {
		color("#ff22cc")cylinder(d = coil[2], h = coil[3]);
	}

	// Create the bottom tier and translate down
	translate([0, 0, 0.02 - (coil[3] + coil[5])]){
		color("#aa00dd")cylinder(d = coil[4], h = coil[5]);
	}
}

//	Coil Pocket
module coil_pocket(coil){
    // Compute the body height
    body_height = coil[1] + coil[3] + coil[5] + $b_thickness;
    body_width = coil[0] + $coil_spacing;

	difference(){
		cube([body_width, body_width, body_height]);
		translate([body_width / 2, body_width / 2, body_height - coil[1] + 0.01]) {
			coil_indent(coil);
		}
	}
}

//	Grid of Coil Pockets
module grid(depth, width, coil){
    body_width = coil[0] + $coil_spacing;
	for(d = [1 : depth]){
		for(w = [1 : width]){
			translate([(w - 1) * body_width,(d - 1) * body_width, 0]){
				coil_pocket(coil);
			}
		}
	}
}

//	Create all final parts
module coil_holder(depth, width, coil=saionara, body=true, lid=true, tool=true, hinge=false){
    // Compute the body height
    body_height = coil[1] + coil[3] + coil[5] + $b_thickness;
    body_width = coil[0] + $coil_spacing;

	if(body == true){
	    union(){
	        intersection(){
	            union(){
					cube([$lid_side_thickness, (depth * body_width) + ($lid_side_thickness * 2), body_height]);
					translate([$lid_side_thickness + width * body_width, 0, 0]){
						cube([$lid_side_thickness, (depth * body_width) + ($lid_side_thickness * 2), body_height]);
					}
	                translate([$lid_side_thickness, 0, 0]){
						cube([width * body_width, $lid_side_thickness, body_height]);
					}
	                translate([$lid_side_thickness,$lid_side_thickness,0]){
						grid(depth, width, coil);
					}
	                translate([$lid_side_thickness, depth * body_width + $lid_side_thickness, 0]){
						cube([width * body_width, $lid_side_thickness, body_height]);
					}
	            }
	            rounded_shell(body_width * depth + $lid_side_thickness * 2, body_width * width + $lid_side_thickness * 2, body_height, $corner_radius);
	        }
			if(lid == true || hinge == true){
		        translate([body_width * width + $lid_side_thickness * 2,body_width * depth / 2 + $lid_side_thickness,body_height]){
					hinge(depth, width, "side", body_width);
				}
			}
	    }
	}

	if(lid == true){
		translate([0, -$part_spacing, $coil_clearance + $lid_top_thickness]){
			rotate([180, 0, 0])lid(depth, width, body_height, body_width);
		}
	}
	if(tool == true){
		translate([-$part_spacing, $part_spacing, 0]){
			tool(depth, body_width);
		}
	}
}
