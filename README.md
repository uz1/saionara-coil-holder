# Saionara Coil Holder

This project aims to use OpenSCAD to design a coil holder of configurable
size. It is very much still a work in progress.

## Getting Started
To use this project you will need [OpenSCAD](http://www.openscad.org/downloads.html) or another script-based modeller that supports the OpenSCAD language.

Once the Sai_Coil_Holder.scad file is loaded into OpenSCAD, you can change the output by modifying the coil_holder() call on line 4. Eamples on how to configure are below:

A 3 x 5 holder with lid and tool
```javascript
coil_holder(3, 5);
```
A 4 x 4 holder without a lid or tool
```javascript
coil_holder(4, 4, lid=false, tool=false);
```
A 2 x 3 holder body with a hinge but without a lid
```javascript
coil_holder(2, 3, lid=false, hinge=true);
```
A lid for a 4 x 5 holder without a body or tool
```javascript
coil_holder(4, 5, body=false, tool=false);
```

The below are
optional flags to include / exclude each component :


| Flag | Description |
| ------ | ------ |
| body | Setting this to false will exclude the main body. Can be used to print the lid or tool independent of the body. |
| tool | Default is true, if set the false the tool will not be included |
| lid | If set to false the lid will not be included and the body will default to not having a hinge. Set the hinge flag to true if you want to body to print with a hinge despite the lid flag value |
| hinge | Only determines if the hinge appears on the body when the lid flag is set to false |
| coil | Sets the coil profiles listed in the coil profile section. Default value is saionara. |


## Supported Coils
Currently the project only supports the Saionara coils and the default
dimensions are build accordingly. However, I do intend to support Sequoia
coils once I source a Sequoia and am able to take measurements and test fit.


## Thanks
Thank you for taking a look, as I continue to learn OpenSCAD, I will make enhancements to the holder and will do my best to track and respond to any issues and merge requests.
